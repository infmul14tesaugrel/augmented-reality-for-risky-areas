#include "filereader.h"
#include <QFile>
#include <QDebug>
#include <QFileInfo>
#include <QHttpMultiPart>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QString>
#include <QDateTime>
#include <QNetworkReply>

//upload POI with photo
void FileReader::multipartUpload(const QString &filename, const QString &title, const QString &lat, const QString &lon, const QString &description)
{
    QNetworkAccessManager *am = new QNetworkAccessManager(this);
    QString path(filename);

    //timestamp
    QString timestamp_s;
    QDateTime* date = new QDateTime;
    qint64 timestamp = date->currentMSecsSinceEpoch();
    timestamp_s = QString::number(timestamp);

    //request
    QNetworkRequest request(QUrl("http://arra.altervista.org/upload.php")); //our server with php-script
    //boundary
    QString bound="margin"; //name of the boundary

    //action
    QByteArray data(QString("--" + bound + "\r\n").toLatin1());
    data.append("Content-Disposition: form-data; name=\"action\"\r\n\r\n");
    data.append("upload.php\r\n");

    //image
    data.append("--" + bound + "\r\n");   //according to rfc 1867
    data.append("Content-Disposition: form-data; name=\"uploaded\"; filename=\"image");
    data.append(timestamp_s); //append timestamp
    data.append(".jpg\"\r\n");  //name of the input is "uploaded" in my form, next one is a file name.
    data.append("Content-Type: image/jpeg\r\n\r\n"); //data type
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly))
        return;
    data.append(file.readAll());   //let's read the file
    data.append("\r\n");

    //title
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"title\"\r\n\r\n");
    data.append(title+"\r\n");

    //lat
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"lat\"\r\n\r\n");
    data.append(lat+"\r\n");

    //lon
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"lon\"\r\n\r\n");
    data.append(lon+"\r\n");

    //imagePOI
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"imagepoi\"\r\n\r\n");
    data.append("image"+timestamp_s+".jpg"+"\r\n");

    //description
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"description\"\r\n\r\n");
    data.append(description+"\r\n");

    //closing boundary
    data.append("--" + bound + "--\r\n");

    request.setRawHeader(QString("Content-Type").toLatin1(),QString("multipart/form-data; boundary=" + bound).toLatin1());
    request.setRawHeader(QString("Content-Length").toLatin1(), QString::number(data.length()).toLatin1());
    QNetworkReply *reply =  am->post(request,data);
    qDebug() << data.data();
    QObject::connect(am, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
}

//update poi with photo
void FileReader::multipartUpload(const QString &filename, const QString &title, const QString &description, const QString &id)

{
    QNetworkAccessManager *am = new QNetworkAccessManager(this);
    QString path(filename);

    //timestamp
    QString timestamp_s;
    QDateTime* date = new QDateTime;
    qint64 timestamp = date->currentMSecsSinceEpoch();
    timestamp_s = QString::number(timestamp);

    //request
    QNetworkRequest request(QUrl("http://arra.altervista.org/update.php")); //our server with php-script
    //boundary
    QString bound="margin"; //name of the boundary

    //action
    QByteArray data(QString("--" + bound + "\r\n").toLatin1());
    data.append("Content-Disposition: form-data; name=\"action\"\r\n\r\n");
    data.append("upload.php\r\n");

    //image
    data.append("--" + bound + "\r\n");   //according to rfc 1867
    data.append("Content-Disposition: form-data; name=\"uploaded\"; filename=\"image");
    data.append(timestamp_s); //append timestamp
    data.append(".jpg\"\r\n");  //name of the input is "uploaded" in my form, next one is a file name.
    data.append("Content-Type: image/jpeg\r\n\r\n"); //data type
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly))
        return;
    data.append(file.readAll());   //let's read the file
    data.append("\r\n");

    //title
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"title\"\r\n\r\n");
    data.append(title+"\r\n");

    //imagePOI
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"imagepoi\"\r\n\r\n");
    data.append("image"+timestamp_s+".jpg"+"\r\n");

    //description
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"description\"\r\n\r\n");
    data.append(description+"\r\n");

    //id
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"id\"\r\n\r\n");
    data.append(id+"\r\n");

    //closing boundary
    data.append("--" + bound + "--\r\n");

    request.setRawHeader(QString("Content-Type").toLatin1(),QString("multipart/form-data; boundary=" + bound).toLatin1());
    request.setRawHeader(QString("Content-Length").toLatin1(), QString::number(data.length()).toLatin1());
    QNetworkReply *reply =  am->post(request,data);
    qDebug() << data.data();
    QObject::connect(am, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
}

//update POI  with no photo
void FileReader::multipartUpload(const QString &title, const QString &description, const QString &id)
{
    QNetworkAccessManager *am = new QNetworkAccessManager(this);

    //timestamp
    QString timestamp_s;
    QDateTime* date = new QDateTime;
    qint64 timestamp = date->currentMSecsSinceEpoch();
    timestamp_s = QString::number(timestamp);

    //request
    QNetworkRequest request(QUrl("http://arra.altervista.org/updatenophoto.php")); //our server with php-script
    //boundary
    QString bound="margin"; //name of the boundary

    //action
    QByteArray data(QString("--" + bound + "\r\n").toLatin1());
    data.append("Content-Disposition: form-data; name=\"action\"\r\n\r\n");
    data.append("upload.php\r\n");

    //title
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"title\"\r\n\r\n");
    data.append(title+"\r\n");

    //description
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"description\"\r\n\r\n");
    data.append(description+"\r\n");

    //id
    data.append("--" + bound + "\r\n");
    data.append("Content-Disposition: form-data; name=\"id\"\r\n\r\n");
    data.append(id+"\r\n");

    //closing boundary
    data.append("--" + bound + "--\r\n");

    request.setRawHeader(QString("Content-Type").toLatin1(),QString("multipart/form-data; boundary=" + bound).toLatin1());
    request.setRawHeader(QString("Content-Length").toLatin1(), QString::number(data.length()).toLatin1());
    QNetworkReply *reply =  am->post(request,data);
    qDebug() << data.data();
    QObject::connect(am, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));
}

//print on console the response from server and emit the signals of correct or not correct operation.
void FileReader::replyFinished(QNetworkReply *reply)
{
    reply->open(QIODevice::ReadOnly);

    // if the response is correct
    if(reply->error() == QNetworkReply::NoError)
    {
        QByteArray str=(reply->readAll());
        QString response = QString::fromUtf8(str.data(), str.size());
        //qDebug()<<" re: "<<response;
        emit ok();

    }

    //error sever

    else
        //qDebug()<<"error response server";
        emit notok();


}
