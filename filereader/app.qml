import QtQuick 2.0
import File 1.0

Item {
    width: 300; height: 200

    FileReader {
        id: filereader
        onOk:{
            console.log("ok")
        }
        onNotok: {
            console.log ("not ok")
        }
    }

    Text {
        anchors.centerIn: parent
        text: "Click to read file into console"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: filereader.multipartUpload('arra.png','title', '1','2','description');

    }
}
