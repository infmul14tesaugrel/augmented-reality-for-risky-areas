#ifndef FILEREADER_H
#define FILEREADER_H

#include <QObject>
#include <QNetworkReply>

class FileReader : public QObject
{
    Q_OBJECT

public:

    Q_INVOKABLE void multipartUpload(const QString &filename, const QString &title, const QString &lat, const QString &lon, const QString &description);
    Q_INVOKABLE void multipartUpload(const QString &filename, const QString &title, const QString &description, const QString &id);
    Q_INVOKABLE void multipartUpload(const QString &title, const QString &description, const QString &id);

public slots:
    Q_INVOKABLE void replyFinished(QNetworkReply *reply);

signals:
    Q_INVOKABLE void finished(QNetworkReply*);
    Q_INVOKABLE void ok();
    Q_INVOKABLE void notok();

};

#endif // FILEREADER_H
