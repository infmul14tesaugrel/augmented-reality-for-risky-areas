﻿import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.0
import QtQuick.LocalStorage 2.0
import "Requestsmanager.js" as Requestsmanager
import QtQuick.Dialogs 1.2
import File 1.0


Rectangle{
    id:editPoiScreen
    color: "steelblue"
    opacity: 0
    focus: false

    onFocusChanged:   {
        fadeInEdit.start()
    }

    PropertyAnimation { id: fadeInEdit; target: editPoiScreen; property: "opacity"; to: 100; duration: 10000 }

    MessageDialog {
        id: deleteOfflinePOIConfirm
        title: "Quit Confirmation"
        text: "Are you sure to DELETE offline POI?"
        standardButtons: StandardButton.Yes | StandardButton.No
        property string id: ""
        onYes:  {
            function deleteOfflinePOI(id) {
            var db = LocalStorage.openDatabaseSync("ArraDB", "1.0", "The ARRA LOCAL DB!", 1000000);

            db.transaction(
                 function(tx) {

                     // Delete all added Tables
                     var rs = tx.executeSql('DELETE FROM POI WHERE id = ?', [id]);

                  }
            )}
         deleteOfflinePOI(id)
         editpoi()


        }
        onNo: deleteOfflinePOIConfirm.close()
    }

    MessageDialog {
        id: deletePOIConfirm
        title: "Quit Confirmation"
        text: "Are you sure to DELETE POI?"
        standardButtons: StandardButton.Yes | StandardButton.No
        property string id: ""
        onYes:  {
            Requestsmanager.deletepoi(id,function(resp) {
                editpoi()
            });


        }
        onNo: deleteOfflinePOIConfirm.close()
    }


    MessageDialog {
        id: noUploadedoffline
        title: "No POI uploaded"
        text: "Your POI upload failed. Please, check your connection and retry"
        standardButtons: StandardButton.Ok
        onAccepted: noUploadedoffline.close()
    }

    MessageDialog {
        id: noConnection
        title: "No Connection detected"
        text: "Your network services are not active. Switch on them and retry"
        standardButtons: StandardButton.Ok
        onAccepted: noConnection.close()
    }

    //existing poi area
    Text{
        id:existingPoiText
        color: "#f9f4f4"
        font.pointSize: 18
        height: 18
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.baseline
        text:"Existing Poi:"
    }

    Rectangle{
    id:existingPoiArea
    width: parent.width
    height: parent.height/2
    color:"lightsteelblue"
    anchors.topMargin: 20
    anchors.top: existingPoiText.baseline
//    anchors.baselineOffset: "10"

    Item{
        id: arrayData
        property variant poiArrayName: []
        property variant poiArrayId: []


        Component.onCompleted: {


            Requestsmanager.getpois(function(resp) {
                for (var i=0; i<resp.POI.length; i++)
                {
                    poiArrayName[i] = resp.POI[i].title;
                    poiArrayId[i] = resp.POI[i].poiID;

                }

                for (var i=0; i<poiArrayName.length; i++)
                {   console.log(poiArrayName[i])
                    poiModel.append({"poi":poiArrayName[i],"update": poiArrayId[i], "del":poiArrayId[i]})
                }

            });

        }
    }

    ListModel {
           id: poiModel
    }

    TableView {
           id:poiTable
           width: parent.width
           height: parent.height
           backgroundVisible: false
           model: poiModel
           alternatingRowColors: true

           TableViewColumn{ role: "poi"  ; title: "Poi" ; width: parent.width/3 }
           TableViewColumn{ role: "update"  ; title: "Update" ; width: parent.width/3;
           delegate: Item{
               Button{
                   id:updateButton
                   text: "update"
                   onClicked: {
                       //click update
                       console.log("update button clicked" + styleData.value)
                       editPoiScreen.destroy()
                       updatepoi(styleData.value)
                   }
               }
           }
           }
           TableViewColumn{ role: "del" ; title: "Delete" ; width: parent.width/3;
               delegate: Item{
                   Button{
                       id:deleteButton
                       text: "delete"
                       onClicked: {
                           console.log("delete button clicked" + styleData.value)

                           Requestsmanager.checkconnection(function(resp) {
                           var respcode=resp;
                           if(respcode!=200){
                                               noConnection.open()

                           }
                           else{
                               deletePOIConfirm.id= styleData.value
                               deletePOIConfirm.open()
                           }
                           });
                       }
                   }
               }
           }

    }

    }

    //not uploaded poi area

    Text{
        id:offlinePoiText
        color: "#f9f4f4"
        font.pointSize: 18
        height: 18
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: existingPoiArea.bottom
        text:"Poi to upload:"
    }

    Rectangle{
    id:offlinePoiArea
    width: parent.width
    height: parent.height/4
    color:"lightsteelblue"
    anchors.topMargin: 20
    anchors.top: offlinePoiText.baseline
//    anchors.baselineOffset: "10"

    FileReader {
        id: filereaderoffline
        property string id:""
        onOk:{
            console.log('ok')
            function deleteOfflinePOI2(id) {
            var db = LocalStorage.openDatabaseSync("ArraDB", "1.0", "The ARRA LOCAL DB!", 1000000);

            db.transaction(
                 function(tx) {

                     // Delete all added Tables
//                     var rs = tx.executeSql('DELETE FROM POI WHERE id = ?', [id]);
                     console.log(id)
                     var rs = tx.executeSql('DELETE FROM POI');

                  }
            )}
         deleteOfflinePOI2(id)
         editpoi()
        }
        onNotok: {
            noUploadedoffline.open()
        }
    }

    Item{
        id: arrayDataOffline
        property variant poiArrayNameOffline: []
        property variant poiArrayIdOffline: []


        Component.onCompleted: {

            function showPOIs() {
                var db = LocalStorage.openDatabaseSync("ArraDB", "1.0", "The ARRA LOCAL DB!", 1000000);

                db.transaction(
                    function(tx) {

                        // Show all offline POIs
                        var rs = tx.executeSql('SELECT * FROM POI');

                        for(var i = 0; i < rs.rows.length; i++) {
                            console.log(rs.rows.item(i).response)
                            poiArrayNameOffline[i]= rs.rows.item(i).title
                            poiArrayIdOffline[i]= rs.rows.item(i).id


                        }
                    }
                )
            }

            showPOIs()

            for (var i=0; i<poiArrayNameOffline.length; i++)
            {    console.log(poiArrayNameOffline[i])
                poiModelOffline.append({"poi":poiArrayNameOffline[i].toString(),"upload": poiArrayIdOffline[i].toString(), "del":poiArrayIdOffline[i].toString()})
            }
        }
    }

    ListModel {
           id: poiModelOffline
//             ListElement{ poi: "mare" ; update: "update" ; del: "delete"}
    }

    TableView {
           id:poiTableOffline
           width: parent.width
           height: parent.height
           backgroundVisible: false
           model: poiModelOffline
           alternatingRowColors: true

           TableViewColumn{ role: "poi"  ; title: "Poi" ; width: parent.width/3 }
           TableViewColumn{ role: "upload"  ; title: "Upload" ; width: parent.width/3;
               delegate: Item{
                   Button{
                       id:uploadButtonOffline
                       text: "upload"
                       onClicked: {
                           console.log("upload button offline clicked" + styleData.value)

                           function uploadOfflinePOI(id) {
                               var db = LocalStorage.openDatabaseSync("ArraDB", "1.0", "The ARRA LOCAL DB!", 1000000);

                               db.transaction(
                                   function(tx) {

                                       // Show all offline POIs
                                       var rs = tx.executeSql('SELECT * FROM POI WHERE id = ?', [id]);

                                           console.log(rs.rows.item(0).response)

                                       filereaderoffline.id=Qt.md5(rs.rows.item(0).lat+rs.rows.item(0).lon)
                                       filereaderoffline.multipartUpload(rs.rows.item(0).filename, rs.rows.item(0).title, rs.rows.item(0).lat, rs.rows.item(0).lon, rs.rows.item(0).description);



                                   }
                               )
                           }

                           uploadOfflinePOI(styleData.value)

                       }
                   }
               }
           }
           TableViewColumn{ role: "del" ; title: "Delete" ; width: parent.width/3;
               delegate: Item{
                   Button{
                       id:deleteButtonOffline
                       text: "delete"
                       onClicked: {
                           console.log("delete button offline clicked" + styleData.value)
                           deleteOfflinePOIConfirm.id= styleData.value
                           deleteOfflinePOIConfirm.open()
                       }
                   }
               }
           }

    }

    }







}
