// Requestsmanager.js
function request(verb, param, cb) {
    print('request: ' + verb + ' ' + 'http://arra.altervista.org/auth.php')
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        print('xhr: on ready state change: ' + xhr.readyState)
        if(xhr.readyState === XMLHttpRequest.DONE) {
            if(cb) {
                var res = JSON.parse(xhr.responseText.toString())
                print (xhr.responseText.toString())
                cb(res);
            }
        }
    }
    xhr.open(verb, 'http://arra.altervista.org/auth.php');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); //è stato modificato per l'autenticazione
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.send(param)
}

function register(name, username, password, cb) {
    // POST http://arra.altervista.org/auth.php
    var param = 'tag=register&name='+name+'&email='+username+'&password='+password;
    request('POST', param , cb)
}

function login(username, password, cb) {
    // POST http://arra.altervista.org/auth.php
    var param = 'tag=login&email='+username+'&password='+password;
    request('POST', param , cb)
}


//check connection to http://www.arra.altervista.org
function checkconnection(cb) {

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {

        if(xhr.readyState == 4 && xhr.status == 200) {
            print (xhr.status.toString())
            var res = xhr.status.toString();
            cb(res);
        }else if (xhr.status != 200){
            print (xhr.status.toString())
            var res = xhr.status.toString();
            cb(res);
        }
    }
    xhr.open('GET', 'http://arra.altervista.org/upload.php');
    xhr.send();

}

function getpois(cb) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        print('xhr: on ready state change: ' + xhr.readyState)
        if(xhr.readyState === XMLHttpRequest.DONE) {
            if(cb) {
                var res = JSON.parse(xhr.responseText.toString())
                //print (xhr.responseText.toString());
                //var res = eval(xhr.responseText.toString());
                cb(res);
            }
        }
    }
    xhr.open('GET', 'http://arra.altervista.org/getpois.php');
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.send()
}

function getpoi(id,cb) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        print('xhr: on ready state change: ' + xhr.readyState)
        if(xhr.readyState === XMLHttpRequest.DONE) {
            if(cb) {
                var res = JSON.parse(xhr.responseText.toString())
                cb(res);
            }
        }
    }
    xhr.open('GET', 'http://arra.altervista.org/getpoi.php?id='+id);
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.send()
}

function deletepoi(id, cb) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        print('xhr: on ready state change: ' + xhr.readyState)
        if(xhr.readyState === XMLHttpRequest.DONE) {
            if(cb) {
                var res = xhr.responseText.toString()
                print (xhr.responseText.toString())
                cb(res);
            }
        }
    }
    //var param ="id="+id;
    xhr.open('GET', 'http://arra.altervista.org/delete.php?id='+id);
    xhr.send()
}
