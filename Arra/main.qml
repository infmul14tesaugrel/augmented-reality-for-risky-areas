import QtQuick 2.2
import QtQuick.Window 2.2
import QtQuick.Controls 1.2
import QtMultimedia 5.0
import QtPositioning 5.2
import QtQuick.LocalStorage 2.0
import "Requestsmanager.js" as Requestsmanager
import QtQuick.Dialogs 1.2

Window {
    id:mainWindow
    visible: true
    width: maximumWidth
    height: maximumHeight
    color: "steelblue"

    PropertyAnimation { id: fadeInAdmin; target: adminScreen; property: "opacity"; to: 100; duration: 100000 }
    PropertyAnimation { id: fadeInLogin; target: logScreen; property: "opacity"; to: 100; duration: 100000 }
    PropertyAnimation { id: fadeInReg; target: regScreen; property: "opacity"; to: 100; duration: 100000 }
    PropertyAnimation { id: fadeInContent; target: contentScreen; property: "opacity"; to: 100; duration: 100000 }
    NumberAnimation { id: fadeInFreccia; target: freccia; property: "y"; easing.period: 0.2; easing.amplitude: 0.75; easing.type: Easing.InOutElastic;from: 700; to: 200; duration: 2000 }

    Splash{
        id:splashScreen
        visible:true
        anchors.fill: parent
//        anchors.centerIn: parent.Center
        color: "steelblue"
        opacity: 1
        focus:false

    }

    Login{
        id: logScreen
        visible:false
        anchors.fill: parent
        opacity: 0
         }

    MessageDialog {
        id: incorrectLogin
        title: "Login Error"
        text: "?"
        standardButtons: StandardButton.Ok
        onAccepted: incorrectLogin.close()
    }

    MessageDialog {
        id: incorrectRegistration
        title: "Registration Error"
        text: "?"
        standardButtons: StandardButton.Ok
        onAccepted: incorrectRegistration.close()
    }

    MessageDialog {
        id: noConnection
        title: "No Connection detected"
        text: "Your network services are not active. Switch on them and retry"
        standardButtons: StandardButton.Ok
        onAccepted: noConnection.close()
    }

    Registration{
        id: regScreen
        visible:false
        anchors.fill: parent
        opacity: 0
         }

    Admin{
        id: adminScreen
        visible:false
        opacity: 0
        width: parent.width
        height: Screen.height/10
//        height:200
        enabled: true
        property color buttonAddColor: "steelblue"
        property color buttonEditColor: "steelblue"
        property color buttonViewColor: "steelblue"
        property bool  buttonAddEnab: true
        property bool  buttonEditEnab: true
        property bool  buttonViewEnab: true


        }

    Rectangle{
        id: contentScreen
        width: parent.width
//        height: parent.height - 200
        height: parent.height - (Screen.height/10)
        anchors.top: adminScreen.bottom
        color: "steelblue"
        visible: false
        opacity:0

        Image {
            id: freccia
            width: 600; height: 600
            source:"freccia.png"
            anchors.horizontalCenter: parent.horizontalCenter
            Text {
                id: chooseLabel
                anchors.top: parent.bottom
                 color: "#f9f4f4"
                text: qsTr("Choose an option")
                font.bold: true
                font.pointSize: 25
            }
        }

    }

    signal splashscreen(int r)
    signal registered(string name, string username, string password)
    signal registration()
    signal backlogin()
    signal login (string username,string password)
    signal addpoi()
    signal updatepoi(string poiid)
    signal editpoi()
    signal viewpoi()
    signal avoiduploadbutton()
    signal enableuploadbutton()
    signal avoidupdatebutton()
    signal enableupdatebutton()


   onSplashscreen: {
       console.log(r)

       if(r >=1){
           splashScreen.visible=false
           fadeInAdmin.start(adminScreen.visible=true)
           fadeInContent.start(contentScreen.visible=true)
           fadeInFreccia.start()
       }

       else{
           splashScreen.visible=false
           fadeInLogin.start(logScreen.visible=true)

       }

   }

   onRegistration: {
    //signal b: apre la pagina di registrazione
    logScreen.visible=false
    fadeInReg.start(regScreen.visible=true)

   }

   onRegistered: {
       //signal a: parte dopo la registrazione

       //mettere qui la funziona che va a scrivere il nuovo utente
       var JSONregister ="";

       Requestsmanager.checkconnection(function(resp) {
       var respcode=resp;
       if(respcode!=200){
                           noConnection.open()

       }
       else{
           Requestsmanager.register(name, username, password,function(resp) {
           JSONregister = JSON.stringify(resp);
           print (typeof JSONregister);
           print (JSONregister);
           print(JSON.stringify(resp))
               if(resp.success==1){

                   regScreen.visible=false
                   fadeInLogin.start(logScreen.visible=true)
               }else{
                   incorrectRegistration.text=resp.error_msg
                   incorrectRegistration.open()

               }

           });
       }
       });

   }

   onBacklogin: {
       //signal c: back to login
       regScreen.visible=false
       fadeInLogin.start(logScreen.visible=true)
   }


   onLogin: {
       console.log(username + password )
       var JSONlogin ="";

       Requestsmanager.checkconnection(function(resp) {
       var respcode=resp;
       if(respcode!=200){
                           noConnection.open()

       }
       else{
           Requestsmanager.login(username, password,function(resp) {
           JSONlogin = JSON.stringify(resp);
           print (typeof JSONlogin);
           print (JSONlogin);
           print(JSON.stringify(resp))
           print (resp.success)
               if(resp.success==1){
                   var name= resp.user.name;
                   adminScreen.name=qsTr("Welcome "+name)
                   storeCredentials(name, username, password)
                   logScreen.visible=false
                   fadeInAdmin.start(adminScreen.visible=true)
                   fadeInContent.start(contentScreen.visible=true)
                   fadeInFreccia.start()
               }else{
                   incorrectLogin.text=resp.error_msg
                   incorrectLogin.open()

               }

           });
       }
       });

       function storeCredentials(name, username, password) {
           var db = LocalStorage.openDatabaseSync("ArraDB", "1.0", "The ARRA LOCAL DB!", 1000000);

           db.transaction(
               function(tx) {

               // Add credentials row
               tx.executeSql('INSERT INTO Credentials VALUES(?, ?, ?)', [name, username, password ]);

               }
           )
        }

    }

    onAddpoi: {
        var myAddComponent=Qt.createComponent("Addpoi.qml")
        if(myAddComponent.Status==myAddComponent.Ready){
        var myAddObject=myAddComponent.createObject(contentScreen)
        myAddObject.anchors.fill=contentScreen
        myAddObject.focus=true
        myAddObject.objectName="addPoiItem"
        }
    }

    onUpdatepoi: {
        var myUpdateComponent=Qt.createComponent("Updatepoi.qml")
        if(myUpdateComponent.Status==myUpdateComponent.Ready){
            var myUpdateObject=myUpdateComponent.createObject(contentScreen)
            myUpdateObject.anchors.fill=contentScreen
            myUpdateObject.focus=true
            myUpdateObject.objectName="updatePoiItem"

                    Requestsmanager.getpoi(poiid,function(resp) {
                        var bubu=resp.POI[0].url
                        var url = "http://arra.altervista.org/images/"+bubu.slice(39);
                        var poiID = resp.POI[0].poiID
                        var title = resp.POI[0].title
                        var description = resp.POI[0].description
                        var lat = resp.POI[0].lat
                        var lon = resp.POI[0].lon

                        console.log(url)

                        myUpdateObject.url= url
                        myUpdateObject.poiID= poiID
                        myUpdateObject.title= title
                        myUpdateObject.description= description
                        myUpdateObject.lat= lat
                        myUpdateObject.lon= lon

                    });

        }
    }

    onEditpoi: {
        var myEditComponent=Qt.createComponent("Editpoi.qml")
        var myEditObject=myEditComponent.createObject(contentScreen)
        myEditObject.anchors.fill=contentScreen
        myEditObject.focus=true
        myEditObject.objectName="editPoiItem"
    }

    onViewpoi: {
        var myViewComponent=Qt.createComponent("Viewpoi.qml")
        var myViewobject=myViewComponent.createObject(contentScreen)
        myViewobject.anchors.fill=contentScreen
        myViewobject.focus=true
        myViewobject.objectName="viewPoiItem"
    }

    onAvoiduploadbutton: {
        adminScreen.buttonEditColor="lightsteelblue"
        adminScreen.buttonViewColor="lightsteelblue"

        adminScreen.buttonEditEnab=false
        adminScreen.buttonViewEnab=false

    }
    onEnableuploadbutton: {
        adminScreen.buttonEditColor="steelblue"
        adminScreen.buttonViewColor="steelblue"

        adminScreen.buttonEditEnab=true
        adminScreen.buttonViewEnab=true
    }

    onAvoidupdatebutton: {
        adminScreen.buttonAddColor="lightsteelblue"
//        adminScreen.buttonEditColor="lightsteelblue"
        adminScreen.buttonViewColor="lightsteelblue"

        adminScreen.buttonAddEnab=false
        adminScreen.buttonEditEnab=false
        adminScreen.buttonViewEnab=false
    }
    onEnableupdatebutton: {
        adminScreen.buttonAddColor="steelblue"
//        adminScreen.buttonEditColor="steelblue"
        adminScreen.buttonViewColor="steelblue"

        adminScreen.buttonAddEnab=true
        adminScreen.buttonEditEnab=true
        adminScreen.buttonViewEnab=true
    }
}
