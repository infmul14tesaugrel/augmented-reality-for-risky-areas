import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2

Rectangle{
    color: "steelblue"
    Column{
        id: welcomeContent
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 100
        spacing: 20
        Text {
            color: "#f9f8f8"
            text: qsTr("Welcome to ")
            font.family: "Sans Serif"
            font.pointSize: 45
            anchors.horizontalCenter: parent.horizontalCenter
            }
        Text {
            color: "#f9f4f4"
            text: qsTr("ARRA")
            font.family: "Ubuntu"
            font.bold: true
            font.pointSize: 60
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Rectangle{
            width: 500
            height: 125
            radius: 50
            color: "#f9f4f4"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 150
            TextInput {
                id: usernameText
                property alias usernameText: usernameText
                color: "#383737"
                text: "username"
                horizontalAlignment :TextInput.AlignHCenter
                anchors.fill: parent
                anchors.centerIn: parent
                onFocusChanged: {
                    if(text=="username"){
                        text=""
                    }
                }
            }
        }
        Rectangle{
            width: 500
            height: 125
            radius: 50
            color: "#f9f4f4"
            anchors.horizontalCenter: parent.horizontalCenter
            TextInput {
                id: passwordText
                text: "password"
                color: "#383737"
                anchors.fill: parent
                anchors.centerIn: parent
                horizontalAlignment :TextInput.AlignHCenter
                echoMode: TextInput.Password
                onFocusChanged: {
                    if(focus){
                        text=""
                    }
                }
            }
        }

        Row{
            anchors.horizontalCenter: parent.horizontalCenter
            Rectangle {
                id: loginButton
                radius: 75
                width: 250 ; height: 110 ;

                property color buttonColor: "steelblue"
                signal mypressed()

                Text {
                    id: loginLabel
                    anchors.centerIn: parent
                    color: "#f9f4f4"
                    text: qsTr("Login")
                }


                onMypressed: {
                console.log(usernameText.text, passwordText.text)
                login(usernameText.text, passwordText.text)
                }

                MouseArea{
                    id: loginMouseArea
                    anchors.fill: parent
                    onClicked: loginButton.mypressed()
                    hoverEnabled: true
                }
                color: loginMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
            }

            Rectangle {
                id: registerButton
                radius: 75
                width: 250 ; height: 110 ;

                property color buttonColor: "steelblue"
                signal mypressed()

                Text {
                    id: registerLabel
                    anchors.centerIn: parent
                    color: "#f9f4f4"
                    text: qsTr("Register")
                }

                onMypressed: {
                console.log("Register Button clicked")
                registration()
                }

                MouseArea{
                    id: registerMouseArea
                    anchors.fill: parent
                    onClicked: registerButton.mypressed()
                    hoverEnabled: true
                }
                color: registerMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
            }
        }

        Image {
            id: logoArra
            width: 500; height: 500
            rotation: 30
            source: "arra.png"
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
