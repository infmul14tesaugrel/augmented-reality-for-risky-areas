import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtMultimedia 5.0
import QtPositioning 5.2
import QtQuick.Dialogs 1.2
import File 1.0
import QtQuick.LocalStorage 2.0
import "Requestsmanager.js" as Requestsmanager

Column{
    spacing: 30

    signal afterup()

    onAfterup: {
//                enablebutton()
//                addPoiScreen.destroy()
////                addButton.flag=false
//                editpoi()
        cameraItem.enableuploadbutt()
        previewArea.visible=false
        uploadArea.visible=false
        outputItem.visible=true
        cameraLabel.visible= true
        procedureLabel.visible= true
        //qui
        addPoiScreen.focus=true
        camera.start()
        nometext.text=""
        descrizionetext.text=""
    }

    Row{
        spacing: 30
        Text {
            id: nome
            text: qsTr("Nome:")
            color: "#f9f4f4"
            font.pointSize: 15
        }
        TextField{
            id: nometext
            text:""
        }
    }

    Row{
        spacing: 30
        Text {
            id: descrizione
            text: qsTr("Descrizione:")
            color: "#f9f4f4"
            font.pointSize: 15
        }
        TextField{
            id: descrizionetext
            text:""
        }
    }

//    Row{
//        spacing: 30
//        Text {
//            id: meta3
//            text: qsTr("meta3")
//            color: "#f9f4f4"
//            font.pointSize: 15
//        }
//        TextField{
//            id: meta3text
//            text:""
//        }
//    }

//    Row{
//        spacing: 30
//        Text {
//            id: lastImagePath
//            text: qsTr("LastImagePath:")
//            color: "#f9f4f4"
//            font.pointSize: 15
//        }
//        Text{
//            id: lastImagePathtext
//            visible:false
//            text:camera.imageCapture.capturedImagePath
//        }
//    }

    Row{
        spacing: 30
        Text {
            id: location
            text: qsTr("Place:")
            color: "#f9f4f4"
            font.pointSize: 15
        }
        Text{
            id: locationtext
            visible: true

            PositionSource {
                id: src
                updateInterval: 1000
                active: true
            }
            text:  src.position.coordinate.latitude + " " + src.position.coordinate.longitude
        }
    }

    Row{
        id:uploadButtons
        spacing: 30
        Rectangle {
            id: uploadButton
            radius: 75
            width: 250 ; height: 110 ;

            property color buttonColor: "steelblue"
            signal mypressed()


            FileReader {
                id: filereader

                onOk:{
                    afterup()
                }
                onNotok: {
                    noUploaded.open()
                }
            }

            Text {
                id: acceptLabel
                anchors.centerIn: parent
                color: "#f9f4f4"
                text: qsTr("Upload")
            }

            MessageDialog {
                id: noPosition
                title: "No Position detected"
                text: "Your position services need to be activated to be able to upload a Poi. Please, activate them and retry!"
                standardButtons: StandardButton.Ok
                onAccepted: noPosition.close()
            }

            MessageDialog {
                id: noConnection
                title: "No Connection detected"
                text: "Your network services are not active. The POI is upload to the offline local storage!"
                standardButtons: StandardButton.Ok
                onAccepted: noConnection.close()
            }

            MessageDialog {
                id: noUploaded
                title: "No POI uploaded"
                text: "Your POI upload faild. Please, retray or disable connection and upload offline POI!"
                standardButtons: StandardButton.Ok
                onAccepted: noUploaded.close()
            }
            onMypressed: {
                Requestsmanager.checkconnection(function(resp) {
                var respcode=resp;
                if(src.position.coordinate.latitude.toString()==="NaN" || src.position.coordinate.longitude.toString()==="NaN"){
                    noPosition.open()
                }else if(respcode!=200){
                                    noConnection.open()
                                    var coord = src.position.coordinate;
                                    console.log("Coordinate:", coord.latitude, coord.longitude);
                                    storePOI(camera.imageCapture.capturedImagePath, nometext.text, coord.latitude, coord.longitude, descrizionetext.text);

                                    function storePOI(filename, title, lat, lon, description) {
                                        var db = LocalStorage.openDatabaseSync("ArraDB", "1.0", "The ARRA LOCAL DB!", 1000000);
                                        var id=Qt.md5(lat.toString()+lon.toString)
                                        console.log(id)
                                        db.transaction(
                                            function(tx) {
                                            // Create the table if it doesn't already exist
                                            tx.executeSql('CREATE TABLE IF NOT EXISTS POI(id TEXT, filename TEXT, title TEXT, lat TEXT, lon TEXT, description TEXT)');

                                            // Add credentials row
                                            tx.executeSql('INSERT INTO POI VALUES(?, ?, ?, ?, ?, ?)', [id, filename, title, lat, lon, description ]);

                                            }
                                        )
                                     }

                                    afterup()
                                }
                else{
                //console.log(lastImagePathtext.text + locationtext.text)
                var coord = src.position.coordinate;
                console.log("Coordinate:", coord.latitude, coord.longitude);
                filereader.multipartUpload(camera.imageCapture.capturedImagePath, nometext.text, coord.latitude, coord.longitude, descrizionetext.text);
                //afterup()
                }
                });

            }

            MouseArea{
                id: uploadMouseArea
                anchors.fill: parent
                onClicked: uploadButton.mypressed()
                hoverEnabled: true
            }
            color: uploadMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
        }

        Rectangle {
            id: backButton
            radius: 75
            width: 250 ; height: 110 ;

            property color buttonColor: "steelblue"
            signal mypressed()

            Text {
                id: backLabel
                anchors.centerIn: parent
                color: "#f9f4f4"
                text: qsTr("Back")
            }

            onMypressed: {
                cameraItem.enableuploadbutt()
                previewArea.visible=false
                uploadArea.visible=false
                outputItem.visible=true
                cameraLabel.visible= true
                procedureLabel.visible= true
                //qui
                addPoiScreen.focus=true
                camera.start()
                nometext.text=""
                descrizionetext.text=""

            }

            MouseArea{
                id: backMouseArea
                anchors.fill: parent
                onClicked: backButton.mypressed()
                hoverEnabled: true
            }
            color: backMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
        }
    }
}
