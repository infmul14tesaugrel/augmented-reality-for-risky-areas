import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtMultimedia 5.0

Image {
    focus: visible

    Row{
        id:imageButtons
        anchors.top: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 30
        Rectangle {
            id: acceptButton
            radius: 75
            width: 250 ; height: 110 ;

            property color buttonColor: "steelblue"
            signal mypressed()

            Text {
               id: acceptLabel
               anchors.centerIn: parent
               color: "#f9f4f4"
               text: qsTr("Accept")
            }

            onMypressed: {

                newphotoScreen.visible=false
                previewArea.visible=false
                updatePoiScreen.visible= true
                updatePoiScreen.focus= true

            }

            MouseArea{
                id: acceptMouseArea
                anchors.fill: parent
                onClicked: acceptButton.mypressed()
                hoverEnabled: true
            }
            color: acceptMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
        }


        Rectangle {
            id: rejectButton
            radius: 75
            width: 250 ; height: 110 ;

            property color buttonColor: "steelblue"
            signal mypressed()

            Text {
               id: rejectLabel
               anchors.centerIn: parent
               color: "#f9f4f4"
               text: qsTr("Reject")
            }

           onMypressed: {
               previewArea.visible= false
               cameraLabel.visible= true
               //procedureLabel.visible=true
               outputItem.visible=true
               camera.start()
               newphotoButton.focus=true

            }

            MouseArea{
                id: rejectMouseArea
                anchors.fill: parent
                onClicked: rejectButton.mypressed()
                hoverEnabled: true
            }
            color: rejectMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
        }
    }
}
