import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtMultimedia 5.0

Rectangle{
    id:addPoiScreen
    color: "steelblue"
    opacity: 0
    focus: false

    onWindowChanged: {
        console.log("giro finestra dio cane")
    }

    onFocusChanged:{
    if(addPoiScreen.focus==true){
        console.log("focus guadagnato")
        fadeInAdd.start()
//          camera.start()

    }else if(addPoiScreen.focus==false && previewArea.visible!=true && uploadArea.visible!=true){
        console.log("focus perso")
        addPoiScreen.destroy()
    }
    }


    PropertyAnimation { id: fadeInAdd; target: addPoiScreen; property: "opacity"; to: 100; duration: 10000 }
    PropertyAnimation { id: fadeInPreview; target: previewArea; property: "opacity"; to: 100; duration: 10000 }
    PropertyAnimation { id: fadeInUpload; target: uploadArea; property: "opacity"; to: 100; duration: 10000 }

    Item {
        anchors.fill:parent
        id: cameraItem

//        signal avoidbut()
//        signal enablebut()

//        onAvoidbut: {
//            console.log("avoid button")
//            avoidbutton()
//        }
//        onEnablebut: {
//            console.log("enable button")
//            enablebutton()
//        }

        signal avoiduploadbutt()
        signal enableuploadbutt()

        onAvoiduploadbutt: {
            console.log("avoid button")
            avoiduploadbutton()
        }
        onEnableuploadbutt: {
            console.log("enable button")
            enableuploadbutton()
        }

        Camera {
            id: camera       

            imageCapture {
                onImageCaptured: {
                  cameraItem.avoiduploadbutt()
                    photoPreview.source = preview
                    uploadPreview.source = preview
                    fadeInPreview.start(previewArea.visible= true)
                    cameraLabel.visible= false
                    procedureLabel.visible=false
                    outputItem.visible=false
                    //qui
                    addPoiScreen.focus=false
                }
                onImageSaved: {
                    console.log("Percorso immagine salvata:" + camera.imageCapture.capturedImagePath)
                    camera.stop()
                    }
             }
        }
        Text {
            id: cameraLabel
            height: 20
            color: "#f9f4f4"
            text: qsTr("1. Click on the image to capture:")
            font.pointSize: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: cameraItem.top
        }
        VideoOutput {
            id: outputItem
            source: camera
            focus: visible
            width: cameraItem.width-20
            height: cameraItem.height-125
            rotation: 90
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top:cameraLabel.bottom
                MouseArea {
                    anchors.fill: parent
                    onClicked: camera.imageCapture.capture()
                }
        }
        Text {
            id: procedureLabel
            color: "#f9f4f4"
            text: "2. Preview, Metadata and Upload -> "
            font.pointSize: 20
            height: 20
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: outputItem.bottom
        }
        Rectangle{
            id: previewArea
            visible: false
            opacity: 0
            color: "steelblue"
            anchors.fill: parent

            Preview{
                id: photoPreview
                anchors.topMargin: 50
                anchors.top:parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width - 100
                height: parent.height - 250
            }

        }
        Rectangle{
            id: uploadArea
            visible: false
            opacity: 0
            color: "steelblue"
            anchors.fill: parent
                 Image{
                      id:uploadPreview
                      width: 300
                      height: 400
                      anchors.horizontalCenter: parent.horizontalCenter
                      Text {
                          id: imageLabel
                          height: 20
                          color: "#f9f4f4"
                          text: qsTr("Immagine:")
                          font.pointSize: 15
                          anchors.right: parent.left
                          anchors.rightMargin: 50
                      }
                 }

                Upload{
                     id: uploadMeta
                     anchors.top: uploadPreview.bottom
                     anchors.topMargin: 50
                     anchors.horizontalCenter: parent.horizontalCenter
                }

        }
    }
}



