import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.LocalStorage 2.0
import "Requestsmanager.js" as Requestsmanager

Rectangle{
    id:splashItem
    color: "steelblue"
    anchors.fill: parent



    ParallelAnimation{
        id: splashArraAnimations
        running: false
        PropertyAnimation { id: splashRotation; target: arraSplash; property: "rotation"; easing.type: Easing.OutQuad; from: 0; to: 360; loops: 2;duration: 3000 }
        NumberAnimation { id: splashWidth; target: arraSplash; property: "width"; easing.period: 0.59; easing.amplitude: 2.35; easing.type: Easing.InOutCirc; from: 1; to: 1200; duration: 4000 }
        NumberAnimation { id: splashHeight; target: arraSplash; property: "height"; easing.period: 0.5; easing.amplitude: 2.35; easing.type: Easing.InOutCirc;from: 1; to: 1200; duration: 4000 }

        property var r
        function checkCredentials() {
            var db = LocalStorage.openDatabaseSync("ArraDB", "1.0", "The ARRA LOCAL DB!", 1000000);
            db.transaction(
                function(tx) {
                // Create the database if it doesn't already exist
                tx.executeSql('CREATE TABLE IF NOT EXISTS Credentials(name TEXT, username TEXT, password TEXT)');
                // Create the table if it doesn't already exist
                tx.executeSql('CREATE TABLE IF NOT EXISTS POI(id TEXT, filename TEXT, title TEXT, lat TEXT, lon TEXT, description TEXT)');

                // extract credentials rows
                    var rs = tx.executeSql('SELECT * FROM CREDENTIALS');

                     r = rs.rows.length;
                }
            )
            return r;
        }

        onStarted: {
            //qui si puo chiamare la funzione che controlla la sessione
            console.log("ho iniziato")
            checkCredentials()

        }
        onStopped: {
            //qui poi si aprirà il login ho direttamente la home(sara simile a onLogin) - per adesso va al login
            console.log("ho finito!")
            splashItem.animationsend()
        }
    }

    Component.onCompleted: {
        splashArraAnimations.start()
    }

    signal animationsend()
    onAnimationsend: {
     splashscreen(splashArraAnimations.r)
    }

    Image {
      id: arraSplash
      width: 1; height: 1
      rotation: 0
      source: "arra.png"
      anchors.centerIn: parent
    }
}
