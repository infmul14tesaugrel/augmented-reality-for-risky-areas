import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.2
import QtQuick.LocalStorage 2.0


Column{
    id: menuCol
//    width: parent.width

    property string name: ""

    Text {
        id: welcomeUser
        text: name
        font.bold: true
        color: "#f9f8f8"
        anchors.horizontalCenter: parent.horizontalCenter
         }

    Row{
        id: myMenuBar
        anchors.horizontalCenter: parent.horizontalCenter

//        spacing: 20
//        width: parent.width

        Rectangle {
            id: addButton
            radius: 75
//            width: 250 ; height: 110 ;
//             width: parent.width/4 ; height: parent.width/2 ;
            width: Screen.width/5 ; height: Screen.height/15 ;
            enabled: adminScreen.buttonAddEnab

            property bool flag:false

            property color buttonColor: "steelblue"
            signal mypressed()
            Text {
                id: addLabel
                anchors.centerIn: parent
                color: "#f9f4f4"
                text: qsTr("Add POI")
            }

            onMypressed: {
             if(addButton.flag==false){
                 addpoi()
                 addButton.flag=true
                }
            }
            MouseArea{
                id: addMouseArea
                anchors.fill: parent
                onClicked: addButton.mypressed()
                hoverEnabled: true
            }
            color: addMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : adminScreen.buttonAddColor
        }

        Rectangle {
            id: editButton
            radius: 75
//            width: parent.width/4 ; height: parent.width/2 ;
            width: Screen.width/5 ; height: Screen.height/15 ;
            enabled: adminScreen.buttonEditEnab

            property color buttonColor: "steelblue"
            signal mypressed()
            //qui
//            enabled: false
            Text {
                id: editLabel
                anchors.centerIn: parent
                color: "#f9f4f4"
                text: qsTr("Edit POI")
            }

            onMypressed: {
                addButton.flag=false
                editpoi()
            }

            MouseArea{
                id: editMouseArea
                anchors.fill: parent
                onClicked: editButton.mypressed()
                hoverEnabled: true
            }
            color: editMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : adminScreen.buttonEditColor
        }

        Rectangle {
            id: viewButton
            radius: 75
//            width: parent.width/4 ; height: parent.width/2 ;
            width: Screen.width/5 ; height: Screen.height/15 ;
            enabled: adminScreen.buttonViewEnab

            property color buttonColor: "steelblue"
            signal mypressed()

            Text {
                id: viewLabel
                anchors.centerIn: parent
                color: "#f9f4f4"
                text: qsTr("View POI")
            }

            onMypressed: {
                addButton.flag=false
                viewpoi()
            }

            MouseArea{
                id: viewMouseArea
                anchors.fill: parent
                onClicked: viewButton.mypressed()
                hoverEnabled: true
            }
            color: viewMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : adminScreen.buttonViewColor
        }

        Rectangle {
            id: quitButton
            radius: 75
//            width: parent.width/4 ; height: parent.width/2 ;
            width: Screen.width/5 ; height: Screen.height/15 ;

            property color buttonColorQuit: "steelblue"
            signal mypressed()

            Text {
                id: quitLabel
                anchors.centerIn: parent
                color: "#f9f4f4"
                text: qsTr("*LogOut*")
            }


            MessageDialog {
                id: quitConfirm
                title: "LogOut Confirmation"
                text: "Are you sure to Logout, DELETE SESSION and ERASE offline storage? If you want just to close the application use your smartphone controls."
                standardButtons: StandardButton.Yes | StandardButton.No
                onYes:  {
                    function deleteCredentials() {
                    var db = LocalStorage.openDatabaseSync("ArraDB", "1.0", "The ARRA LOCAL DB!", 1000000);

                    db.transaction(
                         function(tx) {


                             // Delete all added Tables
                             var rs = tx.executeSql('DELETE FROM Credentials');
                             var rs = tx.executeSql('DELETE FROM POI');


                          }
                    )}
                 deleteCredentials()
                 Qt.quit()


                }
                onNo: quitConfirm.close()
            }

            onMypressed: {
                quitConfirm.open()
            }

            MouseArea{
                id: quitMouseArea
                anchors.fill: parent
                onClicked: quitButton.mypressed()
                hoverEnabled: true
            }
            color: quitMouseArea.pressed ? Qt.darker(buttonColorQuit, 1.5) : buttonColorQuit
        }

    }
    Component.onCompleted: {

        function showuser() {
            var db = LocalStorage.openDatabaseSync("ArraDB", "1.0", "The ARRA LOCAL DB!", 1000000);

            db.transaction(
                function(tx) {

                    // Show user
                    var rs = tx.executeSql('SELECT * FROM Credentials');

                        adminScreen.name= qsTr("Welcome "+rs.rows.item(0).name)

                }
            )
        }

        showuser()

    }
}
