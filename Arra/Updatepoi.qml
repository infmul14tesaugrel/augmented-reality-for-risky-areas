import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtMultimedia 5.0
import QtPositioning 5.2
import QtQuick.Dialogs 1.2
import File 1.0
import QtQuick.LocalStorage 2.0
import "Requestsmanager.js" as Requestsmanager

Rectangle{
    id:updatePoiScreen
    color: "steelblue"
    opacity: 0
    focus: false
    property string url: ""
    property string poiID: ""
    property string title: ""
    property string description: ""
    property string lat: ""
    property string lon: ""

    signal afterup()

onAfterup:{
        editpoi()
        updatePoiScreen.destroy()
        cameraItem.enableupdatebutt()
    }

    onFocusChanged:   {
        if(updatePoiScreen.focus==true)
        {fadeInUpload.start()
                    cameraItem.avoidupdatebutt()
                    previewArea.visible=false
                    newphotoScreen.visible=false
                    outputItem.visible=true
                    cameraLabel.visible= true
                    //procedureLabel.visible= true
                    updatePoiScreen.focus=true
                    camera.start()

         console.log("view poi focus guadagnato")
        }
//        else if(updatePoiScreen.focus==false && previewArea.visible==false && newphotoScreen.visible==false){
//         console.log("view poi focus perso")
//         updatePoiScreen.destroy()
//        }
    }


    PropertyAnimation { id: fadeOutEditpreview; target: previewArea; property: "opacity"; to: 0; duration: 10000 }



    Column{
        spacing: 30
        anchors.horizontalCenter: parent.horizontalCenter



        Row{
            spacing: 30
            anchors.horizontalCenter: parent.horizontalCenter


            Image{
                id:imagePreview
                width: 300
                height: 400
                source: url
                Text {
                    id: imageText
                    height: 20
                    color: "#f9f4f4"
                    text: qsTr("Immagine:")
                    font.pointSize: 15
                    anchors.right: parent.left
                    anchors.rightMargin: 50
                }
            }


        }

        Row{
            spacing: 30
            anchors.horizontalCenter: parent.horizontalCenter
            visible: false
            Text {
                id: poiid
                text: qsTr("Id")
                color: "#f9f4f4"
                font.pointSize: 15
            }
            TextField{
                id: poiidtext
                text:  poiID
            }
        }

        Row{
            spacing: 30
            anchors.horizontalCenter: parent.horizontalCenter
            visible: false
            Text {
                id: imagepath
                text: qsTr("Id")
                color: "#f9f4f4"
                font.pointSize: 15
            }
            TextField{
                id: imagepathtext
                text:  ""
            }
        }


        Row{
            spacing: 30
            anchors.horizontalCenter: parent.horizontalCenter

            Text {
                id: nome
                text: qsTr("Nome:")
                color: "#f9f4f4"
                font.pointSize: 15
            }
            TextField{
                id: nometext
                text:title
            }
        }

        Row{
            spacing: 30
            anchors.horizontalCenter: parent.horizontalCenter

            Text {
                id: descrizione
                text: qsTr("Descrizione:")
                color: "#f9f4f4"
                font.pointSize: 15
            }
            TextField{
                id: descrizionetext
                text: description
            }
        }


        Row{
            spacing: 30
            anchors.horizontalCenter: parent.horizontalCenter

            Text {
                id: location
                text: qsTr("Place:")
                color: "#f9f4f4"
                font.pointSize: 15
            }
            Text{
                id: locationtext
                visible: true

                PositionSource {
                    id: src
                    updateInterval: 1000
                    active: true
                }
                text:  lat + " " + lon
            }
        }

        Row{
            id:uploadButtons
            spacing: 30

            Rectangle {
                id: uploadButton

                radius: 75
                width: 250 ; height: 110 ;

                property color buttonColor: "steelblue"
                signal mypressed()


                            FileReader {
                                id: filereader

                                onOk:{
                                    afterup()
                                }
                                onNotok: {
                                    noUploaded.open()
                                }
                            }

                Text {
                    id: acceptLabel
                    anchors.centerIn: parent
                    color: "#f9f4f4"
                    text: qsTr("Upload")
                }


                MessageDialog {
                    id: noConnection
                    title: "No Connection detected"
                    text: "Your network services are not active. The POI is upload to the offline local storage!"
                    standardButtons: StandardButton.Ok
                    onAccepted: noConnection.close()
                }

                MessageDialog {
                    id: noUploaded
                    title: "No Connection detected"
                    text: "Your network services are not active. Switch on them and retry"
                    standardButtons: StandardButton.Ok
                    onAccepted: noUploaded.close()
                }
                onMypressed: {
                                    Requestsmanager.checkconnection(function(resp) {
                                    var respcode=resp;
                                    if(respcode!=200){
                                                        noConnection.open()
                                                    }
                                    else{
                                    //console.log(lastImagePathtext.text + locationtext.text)
                                    var coordi = src.position.coordinate;
                                    console.log("Coordinate:", lat, lon);
                                        console.log(imagepathtext.text)
                                        if(imagepathtext.text==""){
                                            console.log("cazzo")
                                            filereader.multipartUpload(nometext.text, descrizionetext.text, poiidtext.text);
                                        }else{
                                            filereader.multipartUpload(imagepathtext.text, nometext.text, descrizionetext.text, poiidtext.text);

                                        }


                                    }
                                    });

                }

                MouseArea{
                    id: uploadMouseArea
                    anchors.fill: parent
                    onClicked: uploadButton.mypressed()
                    hoverEnabled: true
                }
                color: uploadMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
            }

            Rectangle {
                id: backButton
                radius: 75
                width: 250 ; height: 110 ;

                property color buttonColor: "steelblue"
                signal mypressed()

                Text {
                    id: backLabel
                    anchors.centerIn: parent
                    color: "#f9f4f4"
                    text: qsTr("Back")
                }

                            onMypressed: {
                //                cameraItem.enablebut()
                //                previewArea.visible=false
                //                uploadArea.visible=false
                //                outputItem.visible=true
                //                cameraLabel.visible= true
                //                procedureLabel.visible= true
                //                //qui
                //                addPoiScreen.focus=true
                //                camera.start()
                //                nometext.text=""
                //                descrizionetext.text=""
                                editpoi()
                                updatePoiScreen.destroy()
                                cameraItem.enableupdatebutt()

                            }

                MouseArea{
                    id: backMouseArea
                    anchors.fill: parent
                    onClicked: backButton.mypressed()
                    hoverEnabled: true
                }
                color: backMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
            }

            Rectangle {
                id: newphotoButton
                radius: 75
                width: 250 ; height: 110 ;

                property color buttonColor: "steelblue"
                signal mypressed()

                Text {
                    id: newphotoLabel
                    anchors.centerIn: parent
                    color: "#f9f4f4"
                    text: qsTr("New Photo")
                }

                            onMypressed: {

                                newphotoScreen.visible=true
                                newphotoScreen.focus=true
                            }

                MouseArea{
                    id: newphotoMouseArea
                    anchors.fill: parent
                    onClicked: newphotoButton.mypressed()
                    hoverEnabled: true
                }
                color: newphotoMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
            }
        }
    }
// tutto ok (con Addpoi.qml)
    Rectangle{
        id:newphotoScreen
        anchors.fill:parent
        color: "steelblue"
        opacity: 0
        focus: false
        visible: false

        onFocusChanged:{
            fadeInnewphoto.start()
            camera.start()


        }

        PropertyAnimation { id: fadeInnewphoto; target: newphotoScreen; property: "opacity"; to: 100; duration: 10000 }
        PropertyAnimation { id: fadeInPreview; target: previewArea; property: "opacity"; to: 100; duration: 10000 }
        PropertyAnimation { id: fadeInUpload; target: updatePoiScreen; property: "opacity"; to: 100; duration: 10000 }

        Item {
            anchors.fill:parent
            id: cameraItem

            signal avoidupdatebutt()
            signal enableupdatebutt()

            onAvoidupdatebutt: {
                console.log("avoid button")
                avoidupdatebutton()
            }
            onEnableupdatebutt: {
                console.log("enable button")
                enableupdatebutton()
            }

            Camera {
                id: camera

                imageCapture {
                    onImageCaptured: {

//                        cameraItem.avoidupdatebutt()
                          photoPreview.source = preview
                          imagePreview.source = preview
                          fadeInPreview.start(previewArea.visible= true)
                        //newphotoScreen.visible=false
                        previewArea.visible= true
                          cameraLabel.visible= false
                          //procedureLabel.visible=false
                          outputItem.visible=false
                          //qui
                          updatePoiScreen.focus=false



                    }
                    onImageSaved: {
                        console.log("Percorso immagine salvata:" + camera.imageCapture.capturedImagePath)
                        imagepathtext.text= camera.imageCapture.capturedImagePath.toString()
                        camera.stop()
                        }
                 }
            }
            Text {
                id: cameraLabel
                height: 20
                color: "#f9f4f4"
                text: qsTr("Click on the image to capture:")
                font.pointSize: 20
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: cameraItem.top
            }
            VideoOutput {
                id: outputItem
                source: camera
                focus: visible
                width: cameraItem.width-20
                height: cameraItem.height-125
                rotation: 90
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top:cameraLabel.bottom
                    MouseArea {
                        anchors.fill: parent
                        onClicked: camera.imageCapture.capture()
                    }
            }
//            Text {
//                id: procedureLabel
//                color: "#f9f4f4"
//                text: "2. Preview, Metadata and Upload -> "
//                font.pointSize: 20
//                height: 20
//                anchors.horizontalCenter: parent.horizontalCenter
//                anchors.top: outputItem.bottom
//            }
            Rectangle {
                id: backcamButton
                radius: 75
                width: 250 ; height: 110 ;
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: outputItem.bottom

                property color buttonColor: "steelblue"
                signal mypressed()

                Text {
                    id: backcamLabel
                    color: "#f9f4f4"
                    text: qsTr("Back")
                    anchors.centerIn: parent

                }

                            onMypressed: {
                                newphotoScreen.visible=true
                                newphotoScreen.focus=true
                                updatePoiScreen.visible=true
                                updatePoiScreen.focus=true
                            }

                MouseArea{
                    id: backcamMouseArea
                    anchors.fill: parent
                    onClicked: backcamButton.mypressed()
                    hoverEnabled: true
                }
                color: backcamMouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor
            }
        }
    }
// tutto ok
    Rectangle{
        id: previewArea
        visible: false
        opacity: 100
        color: "steelblue"
        anchors.fill: parent

        Editpreview{
            id: photoPreview
            anchors.topMargin: 50
            anchors.top:parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 100
            height: parent.height - 250
        }

    }

}
