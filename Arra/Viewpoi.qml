import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtMultimedia 5.0
import QtPositioning 5.2
import QtQuick 2.2
import QtQuick.Controls 1.1
import QtWebView 1.0

//import QtWebKit 3.0

Rectangle{
    id:viewPoiScreen
    color: "steelblue"
    opacity: 0
    focus: false

    onFocusChanged:   {
        if(viewPoiScreen.focus==true)
        {fadeInView.start()
         console.log("view poi focus guadagnato")
        }else if(viewPoiScreen.focus==false){
         console.log("view poi focus perso")
         viewPoiScreen.destroy()
        }
    }

    PropertyAnimation { id: fadeInView; target: viewPoiScreen; property: "opacity"; to: 100; duration: 10000 }

//    Text{
//        anchors.horizontalCenter: parent.horizontalCenter
//        text:"TODO: View Poi section"
//    }

    WebView {
        id: webView
        anchors.fill: parent
        url: "https://www.google.com/fusiontables/embedviz?q=select+col4+from+1oS7hijRioolptlZV7Nrwjd7Rk8k6d2MJU2n6L4Uz&viz=MAP&h=false&lat=42.14713345374664&lng=15.732354334354323&t=1&z=7&l=col4&y=2&tmplt=3&hml=TWO_COL_LAT_LNG"
    }

    MouseArea{
        onClicked: console.log("view poi screen clicked")
        anchors.fill:parent
    }




}
